import java.io.File
import com.typesafe.config.{ConfigSyntax, ConfigParseOptions, ConfigFactory, Config}

import scala.io.Source

object Runner extends App {
  val nearbyFile = new File("src/main/resources/testFile.txt")
  println(nearbyFile.toString)
  println(nearbyFile.getAbsolutePath)
  println(nearbyFile.getCanonicalPath)
  Source.fromFile(nearbyFile).getLines().foreach(println)

  val nearbyConf = new File("src/main/resources/super.conf")
  val conf: Config = ConfigFactory.parseFile(nearbyConf)
  println(conf.toString)

  val nearbyProperties = "src/main/resources/super.properties"
  val propertiesUnresolved: Config = loadPropertiesFileUnresolved(nearbyProperties)
  println(propertiesUnresolved.toString)
  val resolvedConf: Config = propertiesUnresolved.resolve()
  println(resolvedConf.toString)

  def loadPropertiesFileUnresolved(propertiesFile: String): Config = {
    // Lets us load .conf with .properties/any extension.
    val options: ConfigParseOptions = ConfigParseOptions.defaults().setSyntax(ConfigSyntax.CONF)
    // Also need to resolve these ones, as they have the typesafe features.
    ConfigFactory.parseFile(new File(propertiesFile), options)
  }
}